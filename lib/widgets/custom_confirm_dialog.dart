import 'package:coolmovies/constants/routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class CustomConfirmDialog {
  Future<void> showConfirmDialog(
      {required BuildContext context, required String id}) {
    return showDialog(
      context: context,
      builder: (context) {
        return Mutation(
          options: MutationOptions(
              document: gql("""mutation MyMutation(\$id: UUID!) {
  deleteMovieReviewById(input: {id: \$id}) {
    clientMutationId
    deletedMovieReviewId
  }
} """)),
          builder: (runMutation, result) {
            return AlertDialog(
              title: const Text('Delete'),
              content: const Text('Are You Sure?'),
              actions: [
                TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: const Text('Cancel'),
                ),
                TextButton(
                  onPressed: () {
                    runMutation({'id': id});
                    Navigator.of(context).pushNamed(homePage);
                  },
                  child: const Text('Delete'),
                )
              ],
            );
          },
        );
      },
    );
  }
}
