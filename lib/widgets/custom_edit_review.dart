import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'dart:developer' as devtools show log;

class CustomEditReview {
  showBottomSheet(
      {required BuildContext context,
      double? height,
      double? width,
      required String id}) {
    TextEditingController _reviewBody = TextEditingController();
    TextEditingController _reviewTitle = TextEditingController();
    TextEditingController _rating = TextEditingController();
    showModalBottomSheet(
      enableDrag: false,
      isScrollControlled: true,
      context: context,
      builder: (context) {
        return Mutation(
          options: MutationOptions(
              document: gql(
                  """mutation MyMutation (\$body: String!, \$rating: Int!, \$title: String!, \$id: UUID!){
  updateMovieReviewById(
    input: {movieReviewPatch: {body: \$body, rating: \$rating, title: \$title}, id: \$id}
  )
  {movieReview {
      body
      title
      rating
    }}
}

"""),
              onCompleted: (data) => devtools.log(data)),
          builder: (runMutation, result) {
            return Wrap(
              children: [
                SizedBox(
                  height: height,
                  width: width,
                  child: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            IconButton(
                              icon: const Icon(Icons.close),
                              onPressed: () => Navigator.of(context).pop(),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        TextField(
                          controller: _reviewTitle,
                          decoration: const InputDecoration(
                            hintText: 'Review Title',
                          ),
                        ),
                        TextField(
                          controller: _reviewBody,
                          decoration: const InputDecoration(
                            hintText: 'Review Body',
                          ),
                        ),
                        TextField(
                          controller: _rating,
                          decoration: const InputDecoration(
                            hintText: 'Use Only Integer numbers from 0 to 5',
                            labelText: 'Rating',
                          ),
                          keyboardType: TextInputType.number,
                          maxLength: 1,
                        ),
                        TextButton(
                          onPressed: () {
                            runMutation({
                              'title': _reviewTitle.text,
                              'id': id,
                              'userID':
                                  'beb2473b-2c31-44a2-81e3-01efd5c7ac6e', //change if i have time to do everything
                              'body': _reviewBody.text,
                              'rating': int.parse(_rating.text),
                            });
                            _rating.clear();
                            _reviewTitle.clear();
                            _reviewBody.clear();
                          },
                          child: const Text('Send Updates'),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            );
          },
        );
      },
    );
  }
}


//7b4c31df-04b3-452f-a9ee-e9f8836013aa