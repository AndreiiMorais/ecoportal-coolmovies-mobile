import 'package:coolmovies/widgets/custom_confirm_dialog.dart';
import 'package:coolmovies/widgets/custom_edit_review.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'dart:developer' as devtools show log;

class MyReviewsPage extends StatefulWidget {
  final CustomEditReview editReview = CustomEditReview();
  final CustomConfirmDialog confirmDialog = CustomConfirmDialog();

  MyReviewsPage({Key? key}) : super(key: key);

  @override
  _MyReviewsPageState createState() => _MyReviewsPageState();
}

class _MyReviewsPageState extends State<MyReviewsPage> {
  @override
  Widget build(BuildContext context) {
    List<String> args =
        ModalRoute.of(context)!.settings.arguments as List<String>;
    return Scaffold(
      appBar: AppBar(
        title: Text(args[1]),
      ),
      body: Query(
        options: QueryOptions(
          document: gql("""query MyQuery {
  allMovieReviews(
    filter: {userReviewerId: {equalTo: "beb2473b-2c31-44a2-81e3-01efd5c7ac6e"}}
  ) {
    nodes {
      rating
      title
      id
    }
  }
}
         """),
        ),
        builder: (result, {fetchMore, refetch}) {
          devtools.log(result.data.toString());
          if (result.hasException) {
            return Text(result.exception.toString());
          }
          if (result.isLoading) {
            return const CircularProgressIndicator.adaptive();
          }

          final queryResult = result.data?['allMovieReviews']['nodes'];
          return ListView.builder(
            itemCount: queryResult.length,
            itemBuilder: (context, index) {
              return ListTile(
                onLongPress: () {
                  widget.editReview.showBottomSheet(
                    context: context,
                    id: queryResult[index]['id'],
                    height: MediaQuery.of(context).size.height * .90,
                  );
                },
                leading: const Text(
                  'ecoPortal',
                ),
                title: Padding(
                  padding: const EdgeInsets.only(bottom: 0),
                  child: Text(
                    '- ${queryResult[index]['title']}',
                  ),
                ),
                subtitle: RatingBarIndicator(
                  unratedColor: Colors.grey,
                  itemSize: 15,
                  rating: (queryResult[index]['rating']).toDouble(),
                  itemBuilder: (context, index) => const Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                ),
                trailing: IconButton(
                  icon: const Icon(Icons.delete),
                  onPressed: () async {
                    await widget.confirmDialog.showConfirmDialog(
                        context: context, id: queryResult[index]['id']);
                  },
                ),
              );
            },
          );
        },
      ),
    );
  }
}
