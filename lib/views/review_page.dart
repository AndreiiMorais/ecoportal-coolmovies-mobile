import 'package:flutter/material.dart';

class ReviewPage extends StatelessWidget {
  final String body;
  final String name;

  const ReviewPage({Key? key, required this.body, required this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            Text("' $body '"),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text('Author: $name'),
            ),
          ],
        ),
      ),
    );
  }
}
