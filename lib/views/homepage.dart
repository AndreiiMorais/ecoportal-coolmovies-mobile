import 'package:coolmovies/constants/routes.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Query(
        options: QueryOptions(
          document: gql("""query AllMovies {
                              allMovies {
                                    nodes {
                                        title
                                        id
                                        releaseDate
                                        movieDirectorId
                                          }
                                        }
                                      }



                                      """),
        ),
        builder: (result, {fetchMore, refetch}) {
          if (result.hasException) {
            return Text(result.exception.toString());
          }
          if (result.isLoading) {
            return const CircularProgressIndicator.adaptive();
          }

          final queryResult = result.data?['allMovies']['nodes'];

          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Text(
                  'Movies',
                  style: Theme.of(context).textTheme.headline3,
                ),
              ),
              ListView.builder(
                shrinkWrap: true,
                itemCount: queryResult.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Container(
                      decoration: const BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 4,
                              color: Colors.grey,
                              blurStyle: BlurStyle.outer)
                        ],
                        borderRadius: BorderRadius.all(
                          Radius.circular(20),
                        ),
                      ),
                      child: ListTile(
                        contentPadding: const EdgeInsets.all(20),
                        onTap: () {
                          List<String> args = [];
                          args.add(queryResult[index]['id']);
                          args.add(queryResult[index]['title']);
                          Navigator.of(context)
                              .pushNamed(detailsPage, arguments: args);
                        },
                        title: Text(
                          queryResult[index]['title'],
                          style: Theme.of(context).textTheme.headline5,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ],
          );
        },
      ),
    );
  }
}
