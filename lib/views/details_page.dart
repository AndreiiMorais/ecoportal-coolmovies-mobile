import 'package:coolmovies/constants/routes.dart';
import 'package:coolmovies/views/review_page.dart';
import 'package:coolmovies/widgets/custom_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:graphql_flutter/graphql_flutter.dart';


import 'package:intl/intl.dart';

class DetailsPage extends StatefulWidget {
  final CustomBottomSheet bottomSheet = CustomBottomSheet();
  DetailsPage({Key? key}) : super(key: key);

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  @override
  Widget build(BuildContext context) {
    List<String> args =
        ModalRoute.of(context)!.settings.arguments as List<String>;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(args[1]),
        backgroundColor: Colors.transparent,
        actions: [
          PopupMenuButton(
            onSelected: (value) =>
                Navigator.of(context).pushNamed(myReviews, arguments: args),
            itemBuilder: (context) {
              return [
                const PopupMenuItem(
                  value: true,
                  child:  Text('My Reviews on This Movie'),
                ),
              ];
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Query(
          options: QueryOptions(
            document: gql(
              """query MyQuery {
        movieById(id: "${args[0]}") {
          title
          releaseDate
          movieDirectorByMovieDirectorId {
        name
        age
          }
          movieReviewsByMovieId {
        nodes {
          body
          rating
          title
          userByUserReviewerId {
            name
            id
          }
        }
          }
        }
      }""",
            ),
          ),
          builder: (result, {fetchMore, refetch}) {
            if (result.isLoading) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else if (result.hasException) {
              return Text(
                result.exception.toString(),
              );
            } else {
              final movie = result.data?['movieById'];
              final reviews =
                  result.data?['movieById']['movieReviewsByMovieId']['nodes'];
              final director =
                  result.data?['movieById']['movieDirectorByMovieDirectorId'];

              int idx = reviews.length;
              List<String> users = [];
              for (var i = 0; i < idx; i++) {
                users.add(result.data?['movieById']['movieReviewsByMovieId']
                    ['nodes'][i]['userByUserReviewerId']['name']);
              }

              
              return Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      movie['title'],
                      style: Theme.of(context).textTheme.headline3,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 10),
                      child:
                          Text('Release Date: ${DateFormat('MM-dd-yyyy').format(
                        DateTime.parse('${movie['releaseDate']}'),
                      )}'),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text('Director: ${director['name']}'),
                    ),
                    ListView.separated(
                      physics: const NeverScrollableScrollPhysics(),
                      separatorBuilder: (context, index) => const Divider(
                        thickness: 4,
                      ),
                      itemCount: reviews.length,
                      shrinkWrap: true,
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      itemBuilder: (context, index) {
                        return ListTile(
                          onTap: () {
                            Navigator.of(context).push(
                              PageRouteBuilder(
                                pageBuilder:
                                    (context, animation, secondaryAnimation) =>
                                        ReviewPage(
                                  body: reviews[index]['body'],
                                  name: users[index],
                                ),
                                transitionDuration:
                                    const Duration(milliseconds: 200),
                                transitionsBuilder: (context, animation,
                                    secondaryAnimation, child) {
                                  return SlideTransition(
                                    position: Tween<Offset>(
                                      begin: const Offset(1.0, 0.0),
                                      end: const Offset(0.0, 0.0),
                                    ).animate(animation),
                                    child: child,
                                  );
                                },
                              ),
                            );
                          },
                          leading: Text(
                            users[index],
                          ),
                          title: Padding(
                            padding: const EdgeInsets.only(bottom: 5),
                            child: Text(
                              '- ${reviews[index]['title']}',
                            ),
                          ),
                          subtitle: RatingBarIndicator(
                            unratedColor: Colors.grey,
                            itemSize: 15,
                            rating: (reviews[index]['rating']).toDouble(),
                            itemBuilder: (context, index) => const Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                          ),
                          trailing: const Padding(
                            padding: EdgeInsets.only(bottom: 25),
                            child: Icon(Icons.arrow_forward_ios),
                          ),
                        );
                      },
                    )
                  ],
                ),
              );
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          widget.bottomSheet.showBottomSheet(
            movieID: args[0],
            context: context,
            height: MediaQuery.of(context).size.height * .90,
          );
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
