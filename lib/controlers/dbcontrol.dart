import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'dart:developer' as devtools show log;

class DbControl {
  Future<ValueNotifier<GraphQLClient>> initializeGraphQL() async {
    await initHiveForFlutter();
    final HttpLink httpLink = HttpLink(
      'http://192.168.1.103:5000/graphql',
    );

    final AuthLink authLink = AuthLink(
      getToken: () async => 'Bearer <YOUR_PERSONAL_ACCESS_TOKEN>',
      // OR
      // getToken: () => 'Bearer <YOUR_PERSONAL_ACCESS_TOKEN>',
    );

    final Link link = authLink.concat(httpLink);

    ValueNotifier<GraphQLClient> client = ValueNotifier(
      GraphQLClient(
        link: link,
        // The default store is the InMemoryStore, which does NOT persist to disk
        cache: GraphQLCache(store: HiveStore()),
      ),
    );

    return client;
  }

  Future<bool> fetchData(BuildContext context) async {
    bool _isLoading = true;
    var client = GraphQLProvider.of(context).value;
    try {
      var result = await client.query(QueryOptions(
        document: gql(r"""
          query AllMovies {
            allMovies {
              nodes {
                id
                title
                movieDirectorId
                userCreatorId
                releaseDate
              }
            }
          }
        """),
      ));

      if (result.data != null) {
        _isLoading = false;
      }
      return _isLoading;
    } catch (e) {
      devtools.log(e.toString());
      return true;
    }
  }

  queryAllMovies(BuildContext context) async {
    var client = GraphQLProvider.of(context).value;
    try {
      QueryResult result = await client.query(QueryOptions(
        document: gql(r"""
         query MyQuery {
          allMovies {
          nodes {
          title
          id
          releaseDate
          movieReviewsByMovieId
          movieDirectorId
    }
  }
}

        """),
      ));
      return result;
    } catch (e) {
      devtools.log(e.toString());
    }
  }
}
