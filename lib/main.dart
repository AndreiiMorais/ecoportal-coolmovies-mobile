import 'package:coolmovies/constants/routes.dart';
import 'package:coolmovies/controlers/dbcontrol.dart';
import 'package:coolmovies/views/details_page.dart';
import 'package:coolmovies/views/homepage.dart';
import 'package:coolmovies/views/my_reviews_page.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';


void main() async {
  await initHiveForFlutter();
  final HttpLink httpLink = HttpLink(
    'http://192.168.1.103:5000/graphql',
  );

  final AuthLink authLink = AuthLink(
    getToken: () async => 'Bearer <YOUR_PERSONAL_ACCESS_TOKEN>',
    // OR
    // getToken: () => 'Bearer <YOUR_PERSONAL_ACCESS_TOKEN>',
  );

  final Link link = authLink.concat(httpLink);

  ValueNotifier<GraphQLClient> client = ValueNotifier(
    GraphQLClient(
      link: link,
      // The default store is the InMemoryStore, which does NOT persist to disk
      cache: GraphQLCache(store: HiveStore()),
    ),
  );

  var app = GraphQLProvider(client: client, child: const MyApp());

  runApp(app);
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        brightness: Brightness.dark,
        textTheme: TextTheme(
          headline3: TextStyle(
            color: Colors.orange[200],
          ),
          headline5: TextStyle(
            color: Colors.orange[200],
          ),
        ),
      ),
      initialRoute: splashPage,
      routes: {
        splashPage: (context) => SplashPage(),
        homePage: (context) => HomePage(),
        detailsPage: (context) => DetailsPage(),
        myReviews: (context) => MyReviewsPage(),
      },
    );
  }
}

class SplashPage extends StatefulWidget {
  SplashPage({Key? key}) : super(key: key);

  final DbControl control = DbControl();

  @override
  State<SplashPage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<SplashPage> {
  // final ValueNotifier<bool> _isLoading = ValueNotifier(true);

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(seconds: 3)).then(
      (_) => Navigator.of(context).pushReplacementNamed(homePage),
      //aqui poderia chamar alguma api de execuçao antes do app iniciar.
    );

    // Future.wait([widget.control.fetchData(context)]).then((value) {
    //   if (value[0] == false) {
    //     Navigator.of(context).pushReplacementNamed(homePage);
    //   } else {
    //     return;
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Center(
          child: SizedBox(
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text(
                  """Thank you for taking the time to take our test. We really appreciate it.
All the information on what is required can be found in the README at the root of this repo.
Please dont spend ages on this and just get through as much of it as you can.
Good luck! :)""",
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 16),
                CircularProgressIndicator.adaptive(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}



